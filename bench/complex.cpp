#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma ide diagnostic   ignored "cert-err58-cpp"

#include <benchmark/benchmark.h>
#include <fi/html/html.hpp>
#include <random>
#include <sstream>
#include <vector>

using namespace fi::html;
using namespace std::literals;

static void bm_elements(benchmark::State& state) {
    auto output = std::stringstream{};

    for (auto _ : state) {
        // clang-format off
        render(output, html {
            head {
                title {"title"sv}
            },
            body {
                h1 {"something"sv},
                h2 {"else"sv},
                p {"entirely"sv},
                a {attrs{href{"http://www.google.com"}},
                   "Google"sv
                }
            }
        });
        // clang-format on
    }

    state.SetBytesProcessed(output.str().size());
    benchmark::DoNotOptimize(output);
}

BENCHMARK(bm_elements);

struct int_coordinate {
    int x;
    int y;
};

auto generate_int_coords(std::size_t count) -> std::vector<int_coordinate> {
    auto rd     = std::random_device{};
    auto engine = std::default_random_engine{rd()};
    auto dist   = std::uniform_int_distribution<int>(-10000, 10000);

    auto result = std::vector<int_coordinate>{};
    result.reserve(count);
    for (; count > 0; --count) {
        result.push_back({dist(engine), dist(engine)});
    }
    return result;
}

static void bm_int_table(benchmark::State& state) {
    auto coords = generate_int_coords(state.range(0));
    auto output = std::stringstream{};

    for (auto _ : state) {
        // clang-format off
        render(output, html {
            head {
                title {"table"sv}
            },
            body {
                table {
                    tr {th {"X Coord"sv}, th {"Y Coord"sv}},
                    for_each(coords.cbegin(), coords.cend(), [](const auto& coord) {
                      return tr {th {std::to_string(coord.x)}, th {std::to_string(coord.y)}};
                    })
                }
            }
        });
        // clang-format on
    }

    state.SetBytesProcessed(output.str().size());
    benchmark::DoNotOptimize(output);
}

BENCHMARK(bm_int_table)->Arg(8)->Arg(16)->Arg(32)->Arg(64)->Arg(128);

struct double_coordinate {
    double x;
    double y;
};

auto generate_double_coords(std::size_t count) -> std::vector<double_coordinate> {
    auto rd     = std::random_device{};
    auto engine = std::default_random_engine{rd()};
    auto dist   = std::uniform_real_distribution<double>(-10000, 10000);

    auto result = std::vector<double_coordinate>{};
    result.reserve(count);
    for (; count > 0; --count) {
        result.push_back({dist(engine), dist(engine)});
    }
    return result;
}

static void bm_double_table(benchmark::State& state) {
    auto coords = generate_double_coords(state.range(0));
    auto output = std::stringstream{};

    for (auto _ : state) {
        // clang-format off
        render(output, html {
            head {
                title {"table"sv}
            },
            body {
                table {
                    tr {th {"X Coord"sv}, th {"Y Coord"sv}},
                    for_each(coords.cbegin(), coords.cend(), [](const auto& coord) {
                        return tr {th {std::to_string(coord.x)}, th {std::to_string(coord.y)}};
                    })
                }
            }
        });
        // clang-format on
    }

    state.SetBytesProcessed(output.str().size());
    benchmark::DoNotOptimize(output);
}

BENCHMARK(bm_double_table)->Arg(8)->Arg(16)->Arg(32)->Arg(64)->Arg(128);

struct person {
    std::string first_name;
    std::string last_name;
};

template <typename Engine>
auto generate_name(Engine& engine) -> std::string {
    auto size_dist = std::uniform_int_distribution<std::size_t>(5, 10);
    auto char_dist = std::uniform_int_distribution<char>(97, 122);

    auto output = std::string{};
    for (auto size = size_dist(engine); size > 0; --size) {
        output.push_back(char_dist(engine));
    }
    return output;
}

auto generate_persons(std::size_t count) -> std::vector<person> {
    auto rd     = std::random_device{};
    auto engine = std::default_random_engine{rd()};

    auto result = std::vector<person>();
    result.reserve(count);
    for (; count > 0; --count) {
        result.push_back({generate_name(engine), generate_name(engine)});
    }
    return result;
}

static void bm_person_table(benchmark::State& state) {
    auto persons = generate_persons(state.range(0));
    auto output  = std::stringstream{};

    for (auto _ : state) {
        // clang-format off
        render(output, html {
            head {
                title {"table"sv}
            },
            body {
                table {
                    tr {th {"First Name"sv}, th {"Last Name"sv}},
                    for_each(persons.cbegin(), persons.cend(), [](const auto& person) {
                      return tr {th {std::cref(person.first_name)}, th {std::cref(person.last_name)}};
                    })
                }
            }
        });
        // clang-format on
    }

    state.SetBytesProcessed(output.str().size());
    benchmark::DoNotOptimize(output);
}

BENCHMARK(bm_person_table)->Arg(8)->Arg(16)->Arg(32)->Arg(64)->Arg(128);

BENCHMARK_MAIN();

#pragma clang diagnostic pop
