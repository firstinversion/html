#pragma once

#include <boost/hana/tuple.hpp>
#include <fi/html/base.hpp>

namespace fi::html {

#define ELEMENT_COMBINATOR(cppname, tagname)                                                                           \
    template <typename... Combinators>                                                                                 \
    struct cppname : base::element_combinator {                                                                        \
        static constexpr char name[] = #tagname;                                                                       \
        explicit cppname(Combinators&&... combinators)                                                                 \
            : children(boost::hana::make_tuple(std::forward<Combinators>(combinators)...)) {}                          \
                                                                                                                       \
        boost::hana::tuple<Combinators...> children;                                                                   \
    };

    ELEMENT_COMBINATOR(html, html)
    ELEMENT_COMBINATOR(head, head)
    ELEMENT_COMBINATOR(title, title)

    ELEMENT_COMBINATOR(body, body)
    ELEMENT_COMBINATOR(h1, h1)
    ELEMENT_COMBINATOR(h2, h2)
    ELEMENT_COMBINATOR(h3, h3)
    ELEMENT_COMBINATOR(p, p)
    ELEMENT_COMBINATOR(a, a)

    ELEMENT_COMBINATOR(div, div)

    ELEMENT_COMBINATOR(caption, caption)
    ELEMENT_COMBINATOR(col, col)
    ELEMENT_COMBINATOR(colgroup, colgroup)
    ELEMENT_COMBINATOR(table, table)
    ELEMENT_COMBINATOR(tbody, tbody)
    ELEMENT_COMBINATOR(td, td)
    ELEMENT_COMBINATOR(tfoot, tfoot)
    ELEMENT_COMBINATOR(th, th)
    ELEMENT_COMBINATOR(thead, thead)
    ELEMENT_COMBINATOR(tr, tr)

#undef ELEMENT_COMBINATOR

}  // namespace fi::html
