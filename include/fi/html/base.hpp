#pragma once

#include <string_view>

namespace fi::html::base {

    struct html_combinator {};

    struct attribute_combinator : html_combinator {};

    struct element_combinator : html_combinator {};

    struct attrs_base {};

    struct control_combinator {};

    struct component_combinator {};

}  // namespace fi::html::base
