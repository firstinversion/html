#pragma once

#include <fi/html/base.hpp>

namespace fi::html {

    template <typename IfTrue, typename IfFalse>
    struct if_ : base::control_combinator {
        explicit if_(bool predicate, IfTrue&& if_true, IfFalse&& if_false)
            : predicate(predicate)
            , true_branch(std::forward<IfTrue>(if_true))
            , false_branch(std::forward<IfFalse>(if_false)) {}

        bool    predicate;
        IfTrue  true_branch;
        IfFalse false_branch;
    };

    template <typename T>
    struct is_if_control_combinator {
        enum { value = false };
    };

    template <typename IfTrue, typename IfFalse>
    struct is_if_control_combinator<if_<IfTrue, IfFalse>> {
        enum { value = true };
    };

    template <typename InputItr, typename Functor>
    struct for_each : base::control_combinator {
        explicit for_each(InputItr first, InputItr last, Functor&& functor)
            : first(first)
            , last(last)
            , functor(std::forward<Functor>(functor)) {}

        InputItr first;
        InputItr last;
        Functor  functor;
    };

    template <typename T>
    struct is_for_each_control_combinator {
        enum { value = false };
    };

    template <typename InputItr, typename Functor>
    struct is_for_each_control_combinator<for_each<InputItr, Functor>> {
        enum { value = true };
    };

}  // namespace fi::html
