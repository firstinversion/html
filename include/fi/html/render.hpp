#pragma once

#include <boost/hana/for_each.hpp>
#include <type_traits>

namespace fi::html {

    // custom type queries

    template <typename T>
    struct is_literal {
        enum { value = false };
    };

    template <>
    struct is_literal<char> {
        enum { value = true };
    };

    template <>
    struct is_literal<char*> {
        enum { value = true };
    };

    template <>
    struct is_literal<const char*> {
        enum { value = true };
    };

    template <>
    struct is_literal<std::string> {
        enum { value = false };
    };

    template <>
    struct is_literal<std::string_view> {
        enum { value = false };
    };

    template <typename T>
    struct is_reference_wrapper {
        enum { value = false };
    };

    template <typename T>
    struct is_reference_wrapper<std::reference_wrapper<T>> {
        enum { value = true };
    };

    template <typename T>
    struct is_reference_wrapper<std::reference_wrapper<const T>> {
        enum { value = true };
    };

    // renderer struct

    template <typename OutStream, typename Value, typename = void>
    struct renderer {};

    // render combinator helper

    template <typename OutStream, typename CombinatorValue,
              typename = std::enable_if_t<std::is_trivial_v<CombinatorValue>>>
    inline void render_combinator(OutStream& out, CombinatorValue value) {
        renderer<OutStream, CombinatorValue>()(out, value);
    }

    template <typename OutStream, typename CombinatorValue,
              typename = std::enable_if_t<!std::is_trivial_v<CombinatorValue>>>
    inline void render_combinator(OutStream& out, const CombinatorValue& value) {
        renderer<OutStream, CombinatorValue>()(out, value);
    }

    // renderer struct specializations

    template <typename OutStream, typename Value>
    struct renderer<OutStream, Value, typename std::enable_if_t<is_literal<Value>::value>> {
        inline void operator()(OutStream& out, char c) { out << c; }
        inline void operator()(OutStream& out, const char* str) { out << str; }
    };

    template <typename OutStream, typename Value>
    struct renderer<OutStream, Value, typename std::enable_if_t<std::is_same_v<std::string, Value>>> {
        inline void operator()(OutStream& out, const std::string& str) { out << str; }
    };

    template <typename OutStream, typename Value>
    struct renderer<OutStream, Value, typename std::enable_if_t<std::is_same_v<std::string_view, Value>>> {
        inline void operator()(OutStream& out, std::string_view str) { out << str; }
    };

    template <typename OutStream, typename Value>
    struct renderer<OutStream, Value, typename std::enable_if_t<is_reference_wrapper<Value>::value>> {
        inline void operator()(OutStream& out, Value wrapper) { out << wrapper.get(); }
    };

    template <typename OutStream, typename Value>
    struct renderer<OutStream, Value, typename std::enable_if_t<std::is_base_of_v<base::attribute_combinator, Value>>> {
        inline void operator()(OutStream& out, const Value& value) {
            out << Value::name << "=\"" << value.value << '"';
        }
    };

    template <typename OutStream, typename Value>
    struct renderer<OutStream, Value, typename std::enable_if_t<std::is_base_of_v<base::attrs_base, Value>>> {
        template <typename... Attributes>
        inline void operator()(OutStream& out, const attrs<Attributes...>& val) {
            boost::hana::for_each(val.attributes, [&](auto attr) {
                out << ' ';
                render_combinator(out, attr);
            });
        }
    };

    template <typename OutStream, typename Value>
    struct renderer<OutStream, Value, typename std::enable_if_t<std::is_base_of_v<base::element_combinator, Value>>> {
        inline void operator()(OutStream& out, const Value& elem) {
            using namespace boost::hana::literals;
            if constexpr (std::is_base_of_v<base::attrs_base, std::decay_t<decltype(elem.children[0_c])>>) {
                out << '<' << Value::name;
                render_combinator(out, elem.children[0_c]);
                out << '>';
                boost::hana::for_each(boost::hana::drop_front(elem.children),
                                      [&](auto child) { render_combinator(out, child); });
                out << "</" << Value::name << '>';
            } else {
                out << '<' << Value::name << '>';
                boost::hana::for_each(elem.children, [&](auto child) { render_combinator(out, child); });
                out << "</" << Value::name << '>';
            }
        }
    };

    template <typename OutStream, typename Value>
    struct renderer<OutStream, Value, typename std::enable_if_t<is_if_control_combinator<Value>::value>> {
        inline void operator()(OutStream& out, const Value& conditional) {
            if (conditional.predicate) {
                render_combinator(out, conditional.true_branch);
            } else {
                render_combinator(out, conditional.false_branch);
            }
        }
    };

    template <typename OutStream, typename Value>
    struct renderer<OutStream, Value, typename std::enable_if_t<is_for_each_control_combinator<Value>::value>> {
        inline void operator()(OutStream& out, const Value& loop) {
            std::for_each(loop.first, loop.last, [&](auto val) {
                renderer<OutStream, decltype(loop.functor(val))>()(out, loop.functor(val));
            });
        }
    };

    template <typename OutStream, typename Value>
    struct renderer<OutStream, Value, typename std::enable_if_t<std::is_base_of_v<base::component_combinator, Value>>> {
        inline void operator()(OutStream& out, const Value& component) { render_combinator(out, component.render()); }
    };

    // render function

    template <typename OutStream>
    inline void default_preamble(OutStream& out) {
        out << "<!DOCTYPE html>";
    }

    template <typename OutStream>
    inline void no_preamble(OutStream& out) {}

    template <typename OutStream, typename Preamble, typename Value,
              typename = std::enable_if_t<std::is_trivial_v<Value>>>
    inline void render(OutStream& out, Preamble preamble, Value value) {
        preamble(out);
        renderer<OutStream, Value>()(out, value);
    }

    template <typename OutStream, typename Preamble, typename Value,
              typename = std::enable_if_t<!std::is_trivial_v<Value>>>
    inline void render(OutStream& out, Preamble preamble, const Value& value) {
        preamble(out);
        renderer<OutStream, Value>()(out, value);
    }

    template <typename OutStream, typename Value, typename = std::enable_if_t<std::is_trivial_v<Value>>>
    inline void render(OutStream& out, Value value) {
        default_preamble(out);
        renderer<OutStream, Value>()(out, value);
    }

    template <typename OutStream, typename Value, typename = std::enable_if_t<!std::is_trivial_v<Value>>>
    inline void render(OutStream& out, const Value& value) {
        default_preamble(out);
        renderer<OutStream, Value>()(out, value);
    }

}  // namespace fi::html
