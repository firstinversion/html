#pragma once

#include <boost/hana/tuple.hpp>
#include <fi/html/base.hpp>

namespace fi::html {

    template <typename... Attributes>
    struct attrs : base::attrs_base {
        explicit attrs(Attributes&&... attributes)
            : attributes(std::forward<Attributes>(attributes)...) {}

        boost::hana::tuple<Attributes...> attributes;
    };

#define ATTRIBUTE_COMBINATOR(cppname, tagname)                                                                         \
    struct cppname : base::attribute_combinator {                                                                      \
        static constexpr char name[] = #tagname;                                                                       \
                                                                                                                       \
        explicit cppname(std::string_view value)                                                                       \
            : value(value) {}                                                                                          \
                                                                                                                       \
        std::string_view value;                                                                                        \
    };

    ATTRIBUTE_COMBINATOR(href, href)

#undef ATTRIBUTE_COMBINATOR

}  // namespace fi::html
