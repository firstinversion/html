#include <catch2/catch.hpp>

#include <fi/html/html.hpp>
#include <functional>
#include <sstream>

using namespace fi::html;
using namespace std::literals;

TEST_CASE("conditional if render") {
    auto output_hello   = std::stringstream{};
    auto output_goodbye = std::stringstream{};

    // clang-format off
    render(output_hello, no_preamble<decltype(output_hello)>,
        if_{true, h1{"hello"sv}, h1{"goodbye"sv}});

    render(output_goodbye, no_preamble<decltype(output_goodbye)>,
        if_{false, h1{"hello"sv}, h2{"goodbye"sv}});
    // clang-format on

    REQUIRE(output_hello.str() == "<h1>hello</h1>");
    REQUIRE(output_goodbye.str() == "<h2>goodbye</h2>");
}

TEST_CASE("for_each render") {
    auto output = std::stringstream{};
    auto items  = std::vector<std::string>{"billy", "bob", "joe"};

    // clang-format off
    render(output, no_preamble<decltype(output)>,
        body {
            for_each{items.cbegin(), items.cend(), [](const auto& val) {
                return h1 {std::cref(val)};
            }}
        });
    // clang-format on

    REQUIRE(output.str() == "<body><h1>billy</h1><h1>bob</h1><h1>joe</h1></body>");
}
