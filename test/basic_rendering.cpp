#include <catch2/catch.hpp>

#include <fi/html/html.hpp>
#include <functional>
#include <sstream>

using namespace fi::html;
using namespace std::literals;

TEST_CASE("render char") {
    auto output = std::stringstream{};
    render(output, no_preamble<decltype(output)>, "hello");

    REQUIRE(output.str() == "hello");
}

TEST_CASE("render string") {
    auto output = std::stringstream{};
    render(output, no_preamble<decltype(output)>, "hello"s);

    REQUIRE(output.str() == "hello");
}

TEST_CASE("render string_view") {
    auto output = std::stringstream{};
    render(output, no_preamble<decltype(output)>, "hello"sv);

    REQUIRE(output.str() == "hello");
}

TEST_CASE("render title") {
    auto output = std::stringstream{};
    // clang-format off
    render(output,
        html {
            head {
                title {"Title Test"sv}
            }
        }
    );
    // clang-format on

    REQUIRE(output.str() == "<!DOCTYPE html><html><head><title>Title Test</title></head></html>");
}

TEST_CASE("render with body sections and reference wrappers") {
    auto output      = std::stringstream{};
    auto title_value = std::string{"Title Test"};

    // clang-format off
    render(output,
        html {
            head {
                title {std::cref(title_value)}
            },
            body {
                h1 {std::ref(title_value)},
                h2 {"2019-12-29"sv},
                p {"hello"sv}
            }
        }
    );
    // clang-format on

    REQUIRE(output.str() ==
            "<!DOCTYPE html><html><head><title>Title Test</title></head><body><h1>Title "
            "Test</h1><h2>2019-12-29</h2><p>hello</p></body></html>");
}

TEST_CASE("render with attributes") {
    auto output = std::stringstream{};

    // clang-format off
    render(output, no_preamble<decltype(output)>,
        a {attrs{href{"http://website.com"sv}},
            "Website"sv
        }
    );
    // clang-format on

    REQUIRE(output.str() == R"(<a href="http://website.com">Website</a>)");
}
