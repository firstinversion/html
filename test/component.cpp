#include <catch2/catch.hpp>

#include <fi/html/html.hpp>
#include <functional>
#include <sstream>

using namespace fi::html;

struct person : base::component_combinator {
    explicit person(std::string_view first_name, std::string_view last_name)
        : first_name(first_name)
        , last_name(last_name) {}

    [[nodiscard]] auto render() const {
        // clang-format off
        return fi::html::div {
            h1 {std::cref(first_name)},
            h2 {std::cref(last_name)}
        };
        // clang-format on
    }

    std::string_view first_name;
    std::string_view last_name;
};

TEST_CASE("component render") {
    auto output = std::stringstream{};

    // clang-format off
    render(output, no_preamble<decltype(output)>,
        person {"someone", "else"});
    // clang-format on

    REQUIRE(output.str() == "<div><h1>someone</h1><h2>else</h2></div>");
}

TEST_CASE("component inside element") {
    auto output = std::stringstream{};

    // clang-format off
    render(output, no_preamble<decltype(output)>,
        body {
            person {"someone", "else"}
        });
    // clang-format on

    REQUIRE(output.str() == "<body><div><h1>someone</h1><h2>else</h2></div></body>");
}
